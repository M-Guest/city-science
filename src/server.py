from flask import Flask
from flask import request
import psycopg2

import json
import os

app = Flask(__name__)

@app.route("/wards")
def wards():
    limit = request.args.get("limit", 20)
    cursor.execute(list_wards, {"limit":limit})
    data = cursor.fetchall()
    wards = []
    for ward in data:
        wards.append(ward[0])
    return json.dumps(wards)


@app.route("/traffic_counts")
def traffic_counts():
    limit = request.args.get("limit", 20)
    ward = request.args.get("wardcode", "")
    if ward is not "":
        cursor.execute(ward_geo_search, {"limit":limit, "ward":ward})
        data = cursor.fetchall()
        return to_feature_collection(data)
    else:
        cursor.execute(all_traffic_counts, {"limit":limit})
        data = cursor.fetchall()
        return to_feature_collection(data)

@app.route("/traffic_counts/<road>")
def road(road):
    cursor.execute(road_sql, {"road":road})
    data = cursor.fetchone()
    if data is  None:
        return "{}", 404
    return json.dumps(data[0])

def load_sql(file):
    with open(file, 'r') as fileHandle:
        data=fileHandle.read()
    return data

def to_feature_collection(data):
    features = []
    for feature in data:
        features.append(feature[0])
    collection = { "type": "FeatureCollection"}
    collection["features"] = features
    status_code = 200
    if len(features) == 0:
        status_code = 404
    return json.dumps(collection), status_code

if __name__ == '__main__':
    postgres_arguments = os.getenv('POSTGRES_ARGS',
      "dbname=postgres user=postgres host=localhost password=postgres port=5432")
    try:
        connection = psycopg2.connect(postgres_arguments)
        cursor = connection.cursor()
    except:
        print("Failed to connect to database")
    all_traffic_counts = load_sql("all_traffic_counts.sql")
    road_sql = load_sql("road.sql")
    ward_geo_search = load_sql("ward_geo_search.sql")
    list_wards = load_sql("wards.sql")
    app.run(host='0.0.0.0', port=8082)
