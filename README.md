## City Science Project

### Installing libraries
* ```pip install flask```
* ```pip install psycopg2```

### Setting up Postgres
* Install Postgres with PostGIS extensions
* Run ```psql -U <user> -h <host> -f scripts/postgis-ext.sql```
* Run ```./ingestShapeFile.sh -h <host> -u <user>```
* Run ```psql -U <user> -h <host> -f scripts/traffic_counts.sql```

### Environment variables
* ```POSTGRES_ARGS``` defaults to "dbname=postgres user=postgres host=localhost password=postgres port=5432"

### Deployment
Deployed to AWS. The server is running on a t2.micro EC2 instance and Postgres
is running within a db.t2.micro RDS instance.

### API Description

### GET /traffic_counts
Query all traffic counts or limit the results by ward.
##### Parameters
* limit (defaults to 20) : Limit the results returned.
* wardcode : Return traffic counts within a wardcode. If no parameter is provided
 this shall fetch all traffic counts up to the limit set.

##### Error codes
* 404 : Ward code was not found.

##### Return
A feature collection as described in the geojson spec. The feature properties
contains all data regarding the traffic counts. E.g.
```
{  
   "type":"FeatureCollection",
   "features":[  
      {  
         "type":"Feature",
         "geometry":{  
            "coordinates":[  
               -3.37083321989978,
               50.900965632814
            ],
            "type":"Point"
         },
         "properties":{  
            "northing":112172,
            "v2AxleRigidHGV":1854,
            "allHGVs":7004,
            "allMotorVehicles":53717,
            "v5AxleArticHGV":1935,
            "linkLength_miles":4.16,
            "lightGoodsVehicles":4851,
            "easting":303700,
            "CP":6023,
            "AADFYear":2000,
            "localAuthority":"Devon",
            "region":"South West",
            "road":"M5",
            "busesCoaches":384,
            "v4or5AxleRigidHGV":173,
            "pedalCycles":0,
            "linkLength_km":6.7,
            "motorcycles":137,
            "estimationMethodDetailed":"Manual count",
            "roadCategory":"TM",
            "endJunction":"27",
            "startJunction":"28",
            "estimationMethod":"Counted",
            "v3AxleRigidHGV":398,
            "v6orMoreAxleArticHGV":1630
         }
      }
   ]
}
```

#### GET /traffic_counts/[road name]
Get traffic counts for a single road.

##### URL
* road : The name of the road.

##### Error codes
* 404 : Road name was not found.

##### Returns
A single feature described in the geojson spec containing all the information about the requested road.

#### GET /wards

##### Returns
Json array containing the wards name and code. E.g.
```
[{"ward_name": "Abbey", "ward_code": "E05000026"},
 {"ward_name": "Alibon", "ward_code": "E05000027"}]
```
