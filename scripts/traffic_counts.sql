DROP TABLE IF EXISTS traffic_counts;
CREATE TABLE traffic_counts (AADFYear INT,
                             CP INT,
                             Estimation_method VARCHAR,
                             Estimation_method_detailed VARCHAR,
                             Region VARCHAR,
                             LocalAuthority VARCHAR,
                             Road VARCHAR,
                             RoadCategory VARCHAR,
                             Easting INT,
                             Northing INT,
                             StartJunction VARCHAR,
                             EndJunction VARCHAR,
                             LinkLength_km DOUBLE PRECISION,
                             LinkLength_miles DOUBLE PRECISION,
                             PedalCycles INT,
                             Motorcycles INT,
                             CarsTaxis INT,
                             BusesCoaches INT,
                             LightGoodsVehicles INT,
                             V2AxleRigidHGV INT,
                             V3AxleRigidHGV INT,
                             V4or5AxleRigidHGV INT,
                             V3or4AxleArticHGV INT,
                             V5AxleArticHGV INT,
                             V6orMoreAxleArticHGV INT,
                             AllHGVs INT,
                             AllMotorVehicles INT,
                             Geom GEOMETRY);
\cd data
\copy traffic_counts(AADFYear,CP,Estimation_method,Estimation_method_detailed,Region,LocalAuthority,Road,RoadCategory,Easting,Northing,StartJunction,EndJunction,LinkLength_km,LinkLength_miles,PedalCycles,Motorcycles,CarsTaxis,BusesCoaches,LightGoodsVehicles,V2AxleRigidHGV, V3AxleRigidHGV,V4or5AxleRigidHGV,V3or4AxleArticHGV,V5AxleArticHGV,V6orMoreAxleArticHGV,AllHGVs,AllMotorVehicles) FROM 'Devon.csv' DELIMITERS ',' CSV HEADER;
\cd ..
-- Transform the grid reference point to WGS84 to match the ESRI shape file
UPDATE traffic_counts SET Geom = ST_Transform(ST_SetSRID(ST_Point(Easting,Northing) ,27700),4326);
