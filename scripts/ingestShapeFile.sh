#!/bin/bash
while [[ $# -gt 1 ]]; do
    key="$1"
    case $key in
        -u|--user)
            user="$2"
            shift
            ;;
        -h|--host)
            host="$2"
            shift
            ;;
        *)
            log Bad arg: $key >&2
            exit 2 # unknown option
            ;;
    esac
    shift # past argument or value
done

shp2pgsql -s 4326 ../data/wards_12-2016/Wards_December_2016_Full_Clipped_Boundaries_in_Great_Britain.shp public.wards | psql -h $host -d postgres -U $user
